/*******************************************************************************

Author		Riad Kaced @FEB22EDA
Language	SKILL (Cadence Design Systems)
Date			Thu Aug  8 00:21:08 PDT 2019
Lint			100 (best is 100)

Use the SKILL finder Manager to generate the documentation from the inline
documetation below

*******************************************************************************/
procedure(FTTgenRandomNumbers(n @optional maxNum "xg")
"@brief Generates a list of random numbers
@param n: The number of random numbers to be generated
@param maxNum: Max value of a random number
@return A list of random integers

This function generates a list of random numbers provided some user inputs.\ 
Returns a random integer between zero and maxNum-1 if provided.
Examples:
FTTgenRandomNumbers(3)
 -> (1227186940 1783523851 32497630)
FTTgenRandomNumbers(3 10)
 -> (5 8 5)
"
	let(((randomL nil))
		; Issue an error when the first argument is not a postive number
		when(negativep(n)
			error("Please provide a postive number")
		)
		; The random() function takes postive integers only
		; In the check below, I have chosen to accept any numbers
		; using numberp() instead of integerp() then comvert
		; the number using fix() prior to calling random()
		if(maxNum && numberp(maxNum) && maxNum>0
			then for(i 1 n randomL=cons(random(fix(maxNum)) randomL))
			else for(i 1 n randomL=cons(random() randomL))
		)
		; return the list
		randomL
	)
)
