#!/opt/anaconda3/bin/python
import tweepy
import sys
import re
import time
import keysAndTokens

# Authenticate to Twitter
auth = tweepy.OAuthHandler(keysAndTokens.API_KEY, keysAndTokens.API_SECRET)
auth.set_access_token(keysAndTokens.ACCESS_TOKEN,keysAndTokens.ACCESS_TOKEN_SECRET)

# Create API object
api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

twtlink = 'PASTE THE TWEET LINK WHICH RETWEETERS YOU WANT TO BLOCK' # paste inside the ' '

regex = r'(?=twitter\.com\/(\w+)\/status\/(\d+))'
twtid = int(re.search(regex, twtlink).group(2))
user_name = re.search(regex, twtlink).group(1)

rtid = api.retweeters(twtid)

for id in rtid:
    api.create_block(id=id)
    #api.create_mute(id=id) IF YOU WANT TO ONLY MUTE THEM
    time.sleep(3)

api.create_block(screen_name=user_name)

print(f"Number of people who retweeted the tweets that you've blocked: {len(rtid)}")
