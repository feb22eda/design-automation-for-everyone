#!/opt/anaconda3/bin/python

# Code Adapted from https://realpython.com/twitter-bot-python-tweepy/

import tweepy
import sys
import keysAndTokens
import datetime
import csv

# Authenticate to Twitter
auth = tweepy.OAuthHandler(keysAndTokens.API_KEY, keysAndTokens.API_SECRET)
auth.set_access_token(keysAndTokens.ACCESS_TOKEN,keysAndTokens.ACCESS_TOKEN_SECRET)

# Create API object
api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
#Test credentaials using the verify_credentials() method
try:
    api.verify_credentials()
    print("Authentication to Riad's Twitter account OK")
except:
    print("Error during authentication")

search = ("#لا_تتكلم_باسمي_يا_بوشاشي")
nbTweets = 10000

def getDoubab():
    counter = 0
    doubabDict = {}
    silverBullet=" ‏يا معشر الذباب لا مفر لكم، الحراك من ورائكم وشعب الأحرار أمامكم. بوشاشي يمثلني ولي ما عجبوش الحال يدز مع الجزائر الجديدة الغاملة #بوشاشي_يمثلني #تروح_كورونا_زيدو_أنتوما #توقفوا_عن_استدعاء_النشطاء ⁩ #يتنحاو_ڤاع #الحراك_مستمر #الحرية_لمعتقلي_الرأي #حراكنا_ثورة_لتغيير_شامل"
    for tweet in tweepy.Cursor(api.search, search).items(nbTweets):
        try:
            print(f"{counter} :Processing User @{tweet.user.screen_name}")
            user = api.get_user(tweet.user.screen_name)
            createAt=user.created_at
            doubabDict[str(user.id_str)]=(str(user.screen_name),
                createAt.strftime('%m-%d-%y'), user.followers_count,
                user.friends_count, user.favourites_count,
                user.statuses_count, user.default_profile,
                user.default_profile_image)
            api.update_status(status = silverBullet, in_reply_to_status_id = tweet.id_str , auto_populate_reply_metadata=True)
            # print(f"Blocking Doubab @{tweet.user.screen_name}")
            #api.report_spam(tweet.user.id, perform_block=True)
            # api.create_block(tweet.user.id)
            counter = counter+1
        except tweepy.TweepError as e:
            print(e.reason)
        except StopIteration:
            break
    return doubabDict


# Python boiler plate call.
if __name__ == "__main__":
    # Quick check on arguments
    if len (sys.argv) < 1:
            print('Usage:  getTwitterUserInfo twitterUser')
            sys.exit(2)
    # Get the Excel file from cmd line arguments
    # twitterUser = sys.argv[1]
    # Run function
    doubabDict= getDoubab()
    # print(doubabDict)
    with open("doubabNew5.csv", "w") as f_output:
        csv_output = csv.writer(f_output)
        csv_output.writerow(['User ID', 'Use Name', 'Date'])
        for key in sorted(doubabDict.keys()):
            csv_output.writerow([key] + list(doubabDict[key]))
