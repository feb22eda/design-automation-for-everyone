# Design Automation For Everyone


In this repository, we are providing design automation codes for the community under the The GNU General Public License v3.0. 
The scripts shared here are production-grade, they have been developed with the highest coding standards and tested for both 
functionality and performance. 

*  All our SKILL codes are 100% Lint. Also all the codes are documented with the built-in Cadence Finder syntax. Please report any bugs or enhancements.
